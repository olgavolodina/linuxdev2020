#define _GNU_SOURCE
#include <dlfcn.h>
#include <errno.h>
#include <string.h>
#include <stdio.h>

typedef int (*orig_unlinkat_type)(int dirfd, const char *pathname, int flags);

int unlinkat(int dirfd, const char *pathname, int flags) {
    orig_unlinkat_type orig_unlinkat;
    if (strstr(pathname, "FIX") != NULL) {
        printf("Can't remove file with FIX!\n");
        errno = EPERM;
        return -1;
    } else {
        orig_unlinkat = (orig_unlinkat_type)dlsym(RTLD_NEXT,"unlinkat");
        return orig_unlinkat(dirfd, pathname, flags);
    }
}
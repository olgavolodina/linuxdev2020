# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-11-23 02:19+0300\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: l10n.c:63
#, c-format
msgid "Choose a number between 0 and 100\n"
msgstr ""

#: l10n.c:71
#, c-format
msgid "Is it greater then %s (y/n)?\n"
msgstr ""

#: l10n.c:82
#, c-format
msgid "It's %s! \n"
msgstr ""

#: l10n.c:78
#, c-format
msgid "Please type y or n\n"
msgstr ""

#: l10n.c:31
#, c-format
msgid ""
"Program guesses number between 1 and 100\n"
" -r prints numbers in roman style\n"
" --help get help\n"
msgstr ""

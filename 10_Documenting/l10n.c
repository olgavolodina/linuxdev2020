#include <stdio.h>
#include <stdlib.h>
#include <libintl.h>
#include <locale.h>
#include <string.h>



#define _(STRING) gettext(STRING)
/**
 * \brief Array of numbers in roman
 *
 * \details This array make such correspondance: arr[number - 1] <--> number in roman. Initialized from intToRoman.inc
 *
 * \note Substraction of 1 is necessary
 */
char * arr[100] = {
    #include "intToRoman.inc"
};

/**
 * \brief Return string which contains roman version of given number
 * @param x Decimal number
 * \return Number in roman version
 */

char * toRoman(int x) {
    return arr[x - 1];
}
void print_help(){
    printf(_("Program guesses number between 1 and 100\n\
 -r prints numbers in roman style\n\
 --help get help\n"));
}

/**
 * \brief Guesses a number from 1 to 100
 * \details Guesses a number by dividing range in half 
 */


void main(int argc, char* argv[]) {

    int roman = 0;
    setlocale (LC_ALL, "");
    bindtextdomain ("l10n", ".");
    textdomain ("l10n");
    

    if (argc > 1) {
        for (int i = 1; i < argc; ++i) {
            if (strcmp(argv[i], "--help") == 0) {
                print_help();
                return;
            }
            
            if (strcmp(argv[i], "-r") == 0) {
                roman = 1;
            }
        }
         
    }
    printf(_("Choose a number between 0 and 100\n"));
    int l = 0;
    int r = 100;
    int middle = 50;
    char c;
    while (1) {
        char buf[10];
        snprintf (buf, sizeof(buf), "%d",middle);
        printf(_("Is it greater then %s (y/n)?\n"), roman ? toRoman(middle) : buf );
        scanf(" %c", &c);
        if (c == 'y') {
            l = middle + 1;
        } else if (c == 'n') {
            r = middle;
        } else {
            printf(_("Please type y or n\n"));
        }
        if (l == r) {
            snprintf (buf, sizeof(buf), "%d",l);
            printf(_("It's %s! \n"), roman ? toRoman(middle) : buf);
            break;
        }
        middle = (r + l) / 2;
    }
}
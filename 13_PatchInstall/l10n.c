#include <stdio.h>

#include <libintl.h>
#include <locale.h>
#include "config.h"

#define _(STRING) gettext(STRING)
#define PATH "../share/locale/"
void main(void) {

    
    setlocale (LC_ALL, "");
    bindtextdomain ("l10n", PATH);
    textdomain ("l10n");



    printf(_("Choose a number between 0 and 100\n"));
    int l = 0;
    int r = 100;
    int middle = 50;
    char c;
    while (1) {
        printf(_("Is it greater then %d (y/n)?\n"), middle);
        scanf(" %c", &c);
        if (c == 'y') {
            l = middle + 1;
        } else if (c == 'n') {
            r = middle;
        } else {
            printf(_("Please type y or n\n"));
        }
        if (l == r) {
            printf(_("It's %d! \n"), l);
            break;
        }
        middle = (r + l) / 2;
    }
}
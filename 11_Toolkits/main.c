#include <glib.h>
#include <stdio.h>

gint compare_int(gconstpointer item1, gconstpointer item2) {
    return ( (guintptr)item1 - (guintptr)item2);
}

void print_iterator(gpointer item) {
    printf("%li \n",  (guintptr)item);
}
int main(int argc, char** argv) {
    if (argc == 1) {
        return 0;
    }
    GHashTable* hash = g_hash_table_new(g_str_hash, g_str_equal);
    const gchar * delimiter = " ";
    gchar ** splitted = g_strsplit(argv[1], delimiter, -1);

    uint i = 0;
    while (splitted[i] != NULL) {
        guintptr value = (guintptr)g_hash_table_lookup(hash, splitted[i]);
        if ((void*)value == NULL) {
            g_hash_table_insert(hash, splitted[i], (void*)1);
            value = (guintptr)g_hash_table_lookup(hash, splitted[i]);
        } else {
            value = value + 1;
            g_hash_table_insert(hash, splitted[i], (void *)value);
        }
        i += 1;
    }
    GList * list = g_hash_table_get_values(hash);
    GList * sorted = g_list_sort(list, (GCompareFunc)compare_int);
    g_list_foreach(sorted, (GFunc)print_iterator, NULL);
    g_hash_table_destroy(hash);
    g_list_free(list);
    g_list_free(sorted);
    g_strfreev(splitted);
    return 0;
}
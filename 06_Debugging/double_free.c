#include <stdlib.h>
#include <string.h>
#include <stdio.h>

void f(void) {
    printf("f");
    return;
}


int main(void) {
    char a = 'A';
    void * p = malloc(sizeof(char));
    memcpy(p, &a, sizeof(char));
    f();
    free(p);
    free(p);
}
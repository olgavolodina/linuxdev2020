#include "stdio.h"
#include "ncurses.h"
#include "stdlib.h"
#include "string.h"

#define DX 3
    
    void main(int argc, const char * argv[]) {
		if (argc != 2) {
			printf("Error: expected parameters 2, but %i were given\n", argc);
			return;		
		}

		FILE *fp;
		fp = fopen(argv[1], "r");
		if (!fp) {
			printf("Error while opening file\n");
			return;
		}
	
        WINDOW *win;
		int c = 0;

		initscr();
		win = newwin(LINES-2*DX, COLS-2*DX, DX, DX);
		printw("File name: %s", argv[1]);
		refresh();
		scrollok(win, TRUE);
		int max_line_length = COLS-3*DX;
		char *line;
		line = (char*)(malloc(max_line_length));
		for (int i  = 0; i < LINES-2*DX; ++i) {
			fgets(line, max_line_length, fp);
			wprintw(win, "%s", line);
		}
		box(win, 0,0);
		wrefresh(win);
		char ch;
		while (true) {
			ch = getchar();
			if (ch == ' ') { 
				if (feof(fp)) {
					wprintw(win, "\n");
				} else {
					fgets(line, max_line_length, fp);
					wprintw(win, "%s", line);
				}
				box(win, 0, 0);
				wrefresh(win);
			} else if (ch == 27) {
				break;
			}
		}
		endwin();

		free(line);
		fclose(fp);
		return;
   
   }
   